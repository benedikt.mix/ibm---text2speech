from ibm_watson import TextToSpeechV1

# you have to create an ibm cloud account for receiving an api key
api_key = 'YoHX2GM9L-Sth8UgtPJh0KeBHO18XjXN9zfLwB91CdFJ'

# api url
url = 'https://stream-fra.watsonplatform.net/text-to-speech/api'

# take text input
text = input('Was möchtest du gesagt bekommen?: ')

# create service instance
text_to_speech = TextToSpeechV1(
    iam_apikey=api_key,
    url=url
)

# get binary output and write it to an mp3 file
with open('voice.mp3', 'wb') as audio_file:
    audio_file.write(
        text_to_speech.synthesize(
            text,
            voice='de-DE_DieterVoice',
            accept='audio/mp3'
        ).get_result().content)